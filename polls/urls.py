from django.conf.urls import url
from . import views
from views import IndexView

app_name = 'polls'
urlpatterns = [
    url(r'^$', IndexView.as_view(), name='index'),
    # url(r'^(?P<actor_id>[0-9]+)/$', views.detail, name='detail'),
    # url(r'^(?P<actor_id>[0-9]+)/results/$', views.results, name='results'),
]