# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('polls', '0001_initial'),
    ]

    operations = [
        migrations.AlterField(
            model_name='actor',
            name='actor_id',
            field=models.SmallIntegerField(serialize=False, primary_key=True),
        ),
        migrations.AlterField(
            model_name='actor',
            name='last_update',
            field=models.DateTimeField(max_length=30),
        ),
    ]
