var Actors = {
	init: function( config ) {
		this.config = config;

		this.setupTemplates();
		this.bindEvents();

		$.ajaxSetup({
			type: 'GET',
			url: $(this).attr('action')
		});

		$('button').remove(); // little lazy
	},

	bindEvents: function() {
		this.config.letterSelection.on( 'change', this.fetchActors );
		this.config.actorsList.on( 'click', 'li', this.displayAuthorInfo );
		this.config.actorInfo.on( 'click', 'span.close', this.closeOverlay );
	},

	setupTemplates: function() {
		this.config.actorListTemplate = Handlebars.compile( this.config.actorListTemplate);
		this.config.actorInfoTemplate = Handlebars.compile( this.config.actorInfoTemplate);

		Handlebars.registerHelper( 'fullName', function( actor ) {
			return actor.first_name + ' ' + actor.last_name;
		});
	},

	fetchActors: function() {
		var self = Actors;

		$.ajax({
			data: self.config.form.serialize(),
			dataType: 'json',
			success: function(results) {
				self.config.actorsList.empty();
				if ( results.data[0] ) {
					self.config.actorsList.append( self.config.actorListTemplate( results.data) );
				} else {
					self.config.actorsList.append('<li>Nothing returned.</li>');
				}
			}
		});
	},

	displayAuthorInfo: function( e ) {
	    var self = Actors;
	    self.config.actorInfo.slideUp( 300 );
	    $.ajax({
	        data: { actor_id: $(this).data( 'actor_id' ) },
	        dataType: 'json',
	    }).then(function(results) {
	        for (var key in results) {
	            theKey = key;
	            theValue = results[key].toString();
	            newValue2 = theValue.replace(/;/g,';<br>')
	            console.log(newValue2);
	            self.config.actorInfo.html( self.config.actorInfoTemplate( {info : newValue2}) ).slideDown(300);
	        }
	    });
	    e.preventDefault();
	},

	closeOverlay: function() {
		Actors.config.actorInfo.slideUp(300);
	}
};

	Actors.init({
		letterSelection: $('#q'),
		form: $('#actors_form'),
		actorListTemplate: $('#actor_list_template').html(),
		actorInfoTemplate: $('#actor_info_template').html(),
		actorsList: $('ul.actors_list'),
		actorInfo: $('div.actor_info')
	});
