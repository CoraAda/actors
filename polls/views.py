from django.shortcuts import get_object_or_404, render
from django.shortcuts import render
from django.views import generic
from django.http import request
from django.http import HttpResponse
from django.http import JsonResponse
from .models import Actor
from django.db import connection


class IndexView(generic.ListView):
    template_name = 'polls/detail.html'
    model = Actor

    def get(self, request, *args, **kwargs):
        field_value = self.request.GET.get('q')
        self.actor_id = self.request.GET.get('actor_id')
        if field_value is not None:
            if Actor.objects.filter(first_name__istartswith=field_value).exists():
                results = Actor.objects.filter(first_name__istartswith=field_value).values('actor_id', 'first_name', 'last_name')
                #print results
                return JsonResponse({'data': list(results)})
        elif self.actor_id is not None:
            return self.actor_id_detail()
        return super(IndexView, self).get(request, *args, **kwargs)    
        
    def actor_id_detail(self):
        cursor = connection.cursor()
        cursor.execute(
            """
            SELECT film_info, first_name, last_name
            FROM actor_info
            WHERE actor_id LIKE %s
            LIMIT 50
            """, [self.actor_id]
            )
        rows = cursor.fetchone()
        #print rows
        return JsonResponse({'data': list(rows)})